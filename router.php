<?php

/**
 * @package   local_impress
 * @copyright 2019 onwards SC Elearning & Software SRL  {@link http://elearningsoftware.ro/}
 */


// To make sure this is working for Windows and Linux
$currentFile = isset($_SERVER['SCRIPT_FILENAME'])? $_SERVER['SCRIPT_FILENAME'] : null;
$currentFile = str_replace('\\', '/', $currentFile);
$currentFile = str_replace('//', '/', $currentFile);

$URL = false;
if(isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI'])) {
    $URL = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
}

if(isset($DB) && isset($USER) && isset($CFG) && isset($CFG->version) &&
        strpos($currentFile, $CFG->dirroot.'/lib/editor/') === false) {
    
    require_once $CFG->dirroot.'/local/impress/lib.php';

    local_impress_routerProcessCurrentFile();
 }
