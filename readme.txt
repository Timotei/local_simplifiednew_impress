De adaugat in course/moodleform_mod.php line ~750
//moodlero
$mform->addElement('header', 'estimatedtime', get_string('estimatedtime', 'local_proactiv'));
$mform->addElement('text', 'timestudent', get_string('timestudent', 'local_proactiv'));
$mform->setType('timestudent', PARAM_RAW);
$mform->addElement('text', 'timeteacher', get_string('timeteacher', 'local_proactiv'));
$mform->setType('timeteacher', PARAM_RAW);       
//moodlero

De adaugat in course/modlib.php line ~676
//moodlero 
$data->timestudent = $cm->timestudent;
$data->timeteacher = $cm->timeteacher;
//moodlero

De adaugat in course/moodleform_mod.php ~374
//moodlero        
$timestudent = intval($data['timestudent']);
if(!empty($timestudent)){
    if(($timestudent == 0) ||(!is_int($timestudent))) {
        $errors['timestudent'] = get_string('mustbenumber','local_proactiv');
    }
}
$timeteacher = intval($data['timeteacher']);
if(!empty($timeteacher)){
    if(($timeteacher == 0) ||(!is_int($timeteacher))) {
        $errors['timeteacher'] = get_string('mustbenumber','local_proactiv');
    }
}           
//moodlero

//moodlero
De adaugat in config.php 
if(file_exists($CFG->dirroot.'/local/proactiv/router.php')) {
  require $CFG->dirroot.'/local/proactiv/router.php';
}
//moodlero