<?php
/**
 * @package   local_impress
 * @copyright 2018 onwards SC Elearning & Software SRL  {@link http://elearningsoftware.ro/}
 */
defined('MOODLE_INTERNAL') || die();

function xmldb_local_impress_upgrade($oldversion) {
    global $CFG, $DB;
    $dbman = $DB->get_manager();

    if ($oldversion < 2019032701) {        

        // Add custom fields to user table
        $table = new xmldb_table('user');
       
        $field = new xmldb_field('gender', XMLDB_TYPE_CHAR, '7', null, XMLDB_NOTNULL, null, '-', 'lastname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }     

        // impress savepoint reached.
        upgrade_plugin_savepoint(true, 2019032701, 'local', 'impress');
    }


    return true;
}