<?php
/**
 * @package   local_impress
 * @copyright 2017 onwards SC Elearning & Software SRL  {@link http://elearningsoftware.ro/}
 */
defined('MOODLE_INTERNAL') || die();

function xmldb_local_impress_install() {
    global $CFG, $DB;
    $dbman = $DB->get_manager();

    // Add custom fields to user table
    $table = new xmldb_table('user');    
     
    $field = new xmldb_field('gender', XMLDB_TYPE_CHAR, '7', null, XMLDB_NOTNULL, null, '-', 'lastname');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    return true;
}
