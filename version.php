<?php

/**
 * @package   local_impress
 * @copyright 2017 onwards SC Elearning & Software SRL  {@link http://elearningsoftware.ro/}
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019032701;
$plugin->requires  = 2016120500; // Requires Moodle 3.2
$plugin->component = 'local_impress';
