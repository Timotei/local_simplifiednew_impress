<?php

/**
 * @package   local_impress
 * @copyright 2017 onwards SC Elearning & Software SRL  {@link http://elearningsoftware.ro/}
 */

// General
$string['pluginname'] = 'Local impress';
$string['timevalue'] = 'Timp(minute)';
$string['estimatedtime'] = 'Timp estimat';
$string['mustbenumber'] = 'Trebuie sa fie un numar';