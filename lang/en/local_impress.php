<?php

/**
 * @package   local_impress
 * @copyright 2017 onwards SC Elearning & Software SRL  {@link http://elearningsoftware.ro/}
 */

// General
$string['pluginname'] = 'Local impress';
$string['timevalue'] = 'Time(minutes)';
$string['estimatedtime'] = 'Estimated time';
$string['mustbenumber'] = 'Must be a number';
$string['timestudent'] = 'Time estimated student(minutes)';
$string['timeteacher'] = 'Time estimated teacher(minutes)';